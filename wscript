# -*- python -*-
VERSION = '0.0.1'
APPNAME = 'gnome-sharing-service'
top = '.'
out = 'build'

import intltool, gnome

def configure(conf):
    conf.check_tool('python')
    conf.check_tool('misc')
    conf.check_python_version((2,6,0))

    conf.check_tool('gnome intltool dbus')

    conf.define('ENABLE_NLS', 1)


    conf.define('HAVE_BIND_TEXTDOMAIN_CODESET', 1)

    conf.define('VERSION', VERSION)
    conf.define('GETTEXT_PACKAGE', "gnome-sharing-service")
    conf.define('PACKAGE', "gnome-sharing-service")
    conf.define('PYEXECDIR', conf.env["PYTHONDIR"]) # i don't know the difference

    # avoid case when we want to install globally (prefix=/usr) but sysconfdir
    # was not specified
    if conf.env['SYSCONFDIR'] == '/usr/etc':
        conf.define('SYSCONFDIR', '/etc')
    else:
        conf.define('SYSCONFDIR', conf.env['SYSCONFDIR'])

    conf.define('prefix', conf.env["PREFIX"]) # to keep compatibility for now

#    conf.sub_config("help")


def set_options(opt):
    # options for disabling pyc or pyo compilation
    opt.tool_options("python")
    opt.tool_options("misc")
    opt.tool_options("gnu_dirs")


def build(bld):
    bld.install_files('${BINDIR}',
                      """src/gnome-share-file
                         src/gnome-sharing-service""",
                      chmod = 0755)


    # set correct flags in defs.py
    bld.new_task_gen("subst",
                     source= "src/gss/defs.py.in",
                     target= "src/gss/defs.py",
                     install_path="${PYTHONDIR}/gss",
                     dict = bld.env
                    )

    bld.install_files('${PYTHONDIR}/gss', 'src/gss/*.py')
    bld.install_files('${PYTHONDIR}/gss/plugins', 'src/gss/plugins/*.py')

    bld.new_task_gen("subst",
                     source= "data/org.gnome.SharingService.service.in",
                     target= "data/org.gnome.SharingService.service",
                     install_path="${DATADIR}/dbus-1/services",
                     dict = bld.env
                    )

#    bld.add_subdirs("po help data")


    def post(ctx):
        # Postinstall tasks:
        # gnome.postinstall_scrollkeeper('hamster-applet') # Installing the user docs
        #gnome.postinstall_schemas('hamster-applet') # Installing GConf schemas
        #gnome.postinstall_icons() # Updating the icon cache
        pass

    bld.add_post_fun(post)


def release(ctx):
    """packaging a version"""
    import Scripting
    Scripting.commands += ['build']
