# -*- coding: utf-8 -*-
#
# Copyright © 2010 daniel g. siegel <dgsiegel@gnome.org>
# Copyright © 2010 Salomon Sickert <sickert@in.tum.de>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
import threading

try:
    import defs
    PLUGIN_PATH = defs.PYTHONDIR + "/gss/plugins/"
except:
    PLUGIN_PATH = "./gss/plugins/"

# --- SuperClass of objects --- #

class Plugin(object):

  name = ""
  description = ""
  icon = ""

  activated = False

  credentials_required = False
  credentials_username = ""
  credentials_password = ""

  progress = 0
  progress_description = ""

  progress_change = threading.Event()

  preferences_widget = False
  account_widget = False

  def check_mime(self, mime):
    if mime in self.supported_filetypes:
      return True
    else:
      return False

  def __call__(self, uris):  # implement share function here
    pass
  
  def get_preferences_ui(self):
    pass
    
  def get_sharing_ui(self):
    pass 


class PluginManager:

  _instances = {}

  def __init__(self):
    if not PLUGIN_PATH in sys.path:
      sys.path.insert(0, PLUGIN_PATH)
    plugins = [fname[:-3] for fname in os.listdir(PLUGIN_PATH) if fname.endswith(".py")]
    for plugin in plugins:
      try:
        __import__(plugin)
      except ImportError as details:
        print "Unable to load " + plugin + ": ", details

  def list_plugins(self):
    result = []
    mime = ["image/jpeg", "image/jpg"]
    for plugin in Plugin.__subclasses__():
      if not plugin in self._instances:
        self._instances[plugin] = plugin(mime)
      result.append(self._instances[plugin])
    return result

  def list_by_mime(self, mime):
    result = []
    for plugin in self.list_plugins():
      if plugin.check_mime(mime):
        result.append(plugin)
    return result

  def get_by_name(self, name):
    for plugin in self.list_plugins():
      if name == plugin.name:
        return plugin
    return False

  def list_by_mimeset(self, mimeset):
    resultlist = []
    for plugin in self.list_plugins():
      okay = True
      for mimetype in mimeset:
        if not plugin.check_mime(mimetype):
          okay = False
          break
      if okay:
        resultlist.append(plugin)
    return resultlist


