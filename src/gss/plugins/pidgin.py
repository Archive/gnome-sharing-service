# -*- coding: utf-8 -*-
#
# Copyright © 2010 daniel g. siegel <dgsiegel@gnome.org>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
import pygtk
pygtk.require("2.0")
import gtk
import dbus

from gss.plugin import Plugin


COLUMN_PIXBUF, COLUMN_NAME, COLUMN_SEARCH, COLUMN_ACCOUNT, COLUMN_ID, COLUMN_VISIBLE = range(6)


class PidginSharing(Plugin):
  name = "Pidgin"
  description = "Pidgin Sharing"
  icon = "pidgin"
  supported_filetypes = []

  activated = False

  purple = None
  widget = None
  treeview = None
  liststore = None
  filter = None

  def __init__(self, mime):
    try:
      bus = dbus.SessionBus()
      obj = bus.get_object("im.pidgin.purple.PurpleService", "/im/pidgin/purple/PurpleObject")
      self.purple = dbus.Interface(obj, "im.pidgin.purple.PurpleInterface")
    except dbus.DBusException:
      return
    self.activated = True

  def get_widget(self):
    if self.widget != None:
      return self.widget

    self.widget = gtk.VBox(False, 5)

    entry = gtk.Entry()
    self.widget.pack_start(entry, False)
    entry.grab_focus()

    scroll = gtk.ScrolledWindow()
    scroll.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
    scroll.set_shadow_type(gtk.SHADOW_IN)
    self.widget.pack_start(scroll, True)

    self.treeview = gtk.TreeView()
    self.treeview.set_headers_visible(False)
    self.treeview.expand_all()
    scroll.add(self.treeview)

    self.liststore = gtk.ListStore(gtk.gdk.Pixbuf, str, str, str, str, bool)
    self.liststore.set_sort_column_id(COLUMN_NAME, gtk.SORT_ASCENDING)

    self.filter = self.liststore.filter_new()
    self.filter.set_visible_column(COLUMN_VISIBLE)
    self.treeview.set_model(self.filter)

    selection = self.treeview.get_selection()
    selection.set_mode(gtk.SELECTION_MULTIPLE)

    img_cell = gtk.CellRendererPixbuf()
    img_column = gtk.TreeViewColumn("Photo", img_cell, pixbuf=COLUMN_PIXBUF)
    self.treeview.append_column(img_column)

    text_cell = gtk.CellRendererText()
    text_column = gtk.TreeViewColumn("Name", text_cell, markup=COLUMN_NAME)
    self.treeview.append_column(text_column)

    theme = gtk.icon_theme_get_default()
    icon = theme.load_icon("avatar-default", 48, 0)

    accounts = self.purple.PurpleAccountsGetAllActive()
    for account in accounts:
      buddies = self.purple.PurpleFindBuddies(account, "")
      for buddy in buddies:
        if self.purple.PurpleBuddyIsOnline(buddy):
          alias = self.purple.PurpleBuddyGetAlias(buddy)
          name = self.purple.PurpleBuddyGetName(buddy)
          i = self.purple.PurpleBuddyGetIcon(buddy)
          if i != 0:
            filename = self.purple.PurpleBuddyIconGetFullPath(i)
            photo = gtk.gdk.pixbuf_new_from_file_at_size(filename, 48, 48)
          else:
            photo = icon

          self.liststore.append([photo, "<b>" + alias + "</b>\n<small>" +
          name + "</small>", name + " " + alias, account, name, True])

    entry.connect("changed", self._update_treeview)

    return self.widget

  def share (self, files):
    (model, selection) = self.treeview.get_selection().get_selected_rows()
    for s in selection:
      iter = self.filter.get_iter_from_string(str(s[0]))
      store_iter = self.filter.convert_iter_to_child_iter(iter)
      account = int(self.liststore.get_value(store_iter, COLUMN_ACCOUNT))
      who = self.liststore.get_value(store_iter, COLUMN_ID)
      connection = int(self.purple.PurpleAccountGetConnection(account))
      if connection != None:
        for filename in files:
          self.purple.ServSendFile(connection, who, filename)


  def _update_treeview (self, data):
    keywords = data.get_text().lower().split(" ")
    iter = self.liststore.get_iter_first()
    while (iter != None):
      val = self.liststore.get_value(iter, COLUMN_SEARCH).lower()
      self.liststore.set_value(iter, COLUMN_VISIBLE, True)
      for k in keywords:
        if k not in val:
          self.liststore.set_value(iter, COLUMN_VISIBLE, False)
      iter = self.liststore.iter_next(iter)
