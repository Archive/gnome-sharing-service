# -*- coding: utf-8 -*-
#
# Copyright © 2010 daniel g. siegel <dgsiegel@gnome.org>
# Copyright © 2010 Salomon Sickert <sickert@in.tum.de>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
import pygtk
pygtk.require("2.0")
import gtk

from gss.plugin import Plugin


class TestPlugin(Plugin):
  name = "Test Plugin"
  description = "This is a sample plugin"
  icon = "gnome-run"
  supported_filetypes = ["image/jpeg", "image/jpg"]

  activated = False

  credentials_required = False
  credentials_username = ""
  credentials_password = ""

  progress = 0
  progress_description = ""

  preferences_widget = False
  account_widget = False

  def __init__(self, mime):
    if mime in self.supported_filetypes:
      self.activated = True
    else:
      self.activated = False
    
    label = gtk.Label ("This is a label")
    self.widget = gtk.VBox(False, 5)
    self.widget.pack_start (label, True)
    
  def get_widget(self):
    return self.widget

  def share (self, files):
    pass
    
