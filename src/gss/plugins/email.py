# -*- coding: utf-8 -*-
#
# Copyright © 2010 daniel g. siegel <dgsiegel@gnome.org>
# Copyright © 2010 Salomon Sickert <sickert@in.tum.de>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import os
import pygtk
pygtk.require("2.0")
import gtk
import evolution

from gss.plugin import Plugin


COLUMN_PIXBUF, COLUMN_NAME, COLUMN_SEARCH, COLUMN_VISIBLE = range(4)


class EmailSharing(Plugin):
  name = "Email"
  description = "Email Sharing"
  icon = "email"
  supported_filetypes = []

  activated = False

  credentials_required = False

  widget = None
  text = None
  treeview = None
  liststore = None
  filter = None

  def __init__(self, mime):
    # We support every mime
    self.activated = True

  def get_widget(self):
    if self.widget != None:
      return self.widget

    self.widget = gtk.VBox(False, 5)

    entry = gtk.Entry()
    self.widget.pack_start(entry, False)
    entry.grab_focus()

    scroll = gtk.ScrolledWindow()
    scroll.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
    scroll.set_shadow_type(gtk.SHADOW_IN)
    self.widget.pack_start(scroll, True)

    self.treeview = gtk.TreeView()
    self.treeview.set_headers_visible(False)
    self.treeview.expand_all()
    scroll.add(self.treeview)

    self.liststore = gtk.ListStore(gtk.gdk.Pixbuf, str, str, bool)
    self.liststore.set_sort_column_id(COLUMN_NAME, gtk.SORT_ASCENDING)

    self.filter = self.liststore.filter_new()
    self.filter.set_visible_column(COLUMN_VISIBLE)
    self.treeview.set_model(self.filter)

    selection = self.treeview.get_selection()
    selection.set_mode(gtk.SELECTION_MULTIPLE)

    img_cell = gtk.CellRendererPixbuf()
    img_column = gtk.TreeViewColumn("Photo", img_cell, pixbuf=COLUMN_PIXBUF)
    self.treeview.append_column(img_column)

    text_cell = gtk.CellRendererText()
    text_column = gtk.TreeViewColumn("Name", text_cell, markup=COLUMN_NAME)
    self.treeview.append_column(text_column)

    theme = gtk.icon_theme_get_default()
    icon = theme.load_icon("avatar-default", 48, 0)

    for addrDesc, addrName in evolution.ebook.list_addressbooks():
      addresses = evolution.ebook.open_addressbook(addrName)

      for contact in addresses.get_all_contacts():
        for i in range(1, 5):
          name = contact.get_property("full-name")
          mail = contact.get_property("email-" + str(i))
          photo = contact.get_photo(48)
          if not photo:
            photo = icon

          if mail:
            self.liststore.append([photo, "<b>" + name + "</b>\n<small>" +
            mail + "</small>", name + " <" + mail + ">", True])

    entry.connect("changed", self._update_treeview)

    return self.widget

  def share (self, files):
    recipients = ""
    (model, selection) = self.treeview.get_selection().get_selected_rows()
    for s in selection:
      iter = self.filter.get_iter_from_string(str(s[0]))
      store_iter = self.filter.convert_iter_to_child_iter(iter)
      val = self.liststore.get_value(store_iter, COLUMN_SEARCH)
      recipients = recipients + " '" + val + "'"
    command = "xdg-email --utf8 --subject 'Shared Files'"
    for file in files:
      command = command + " --attach " + file
    command = command + recipients

    print command
    os.system(command)

  def _update_treeview (self, data):
    keywords = data.get_text().lower().split(" ")
    iter = self.liststore.get_iter_first()
    while (iter != None):
      val = self.liststore.get_value(iter, COLUMN_SEARCH).lower()
      self.liststore.set_value(iter, COLUMN_VISIBLE, True)
      for k in keywords:
        if k not in val:
          self.liststore.set_value(iter, COLUMN_VISIBLE, False)
      iter = self.liststore.iter_next(iter)
