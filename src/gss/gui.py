# -*- coding: utf-8 -*-
#
# Copyright © 2010 daniel g. siegel <dgsiegel@gnome.org>
# Copyright © 2010 Salomon Sickert <sickert@in.tum.de>
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import sys
import pygtk
pygtk.require("2.0")
import gtk
import gobject

import threading

from gss.plugin import PluginManager

    # move to gui as this is a filter
    #mimeset = set()
    #for uri in uris:
    #  (mime, null) = mimetypes.guess_type(uri)
    #  if not mime:
    #    return
    #  mimeset.add(mime)

COLUMN_ICON, COLUMN_NAME = range(2)

class SharingDialog (threading.Thread):

  uris = None
  liststore = None
  treeview = None
  dialog = None
  content = None
  plugin_widget = None
  plugin_selected = None
  plugin_manager = None

  plugin_widget_old = None
  def __init__ (self, uris):
    threading.Thread.__init__(self) 
    self.uris = uris

  def close_application(self, widget=None, event=None, data=None):
    gtk.main_quit()
    return False

  def on_cursor_changed(self, data=None):
    selection = data.get_selection()
    (model, iter) = selection.get_selected()
    name = self.liststore.get_value(iter, COLUMN_NAME)
    if self.plugin_widget_old == name:
      return
    self.plugin_widget_old = name
    print "Loading " + name + " widget"
    plugin = self.plugin_manager.get_by_name(name)
    if plugin:
      plugin_selected = plugin
      if self.plugin_widget != None:
        self.content.remove(self.plugin_widget)
      self.plugin_widget = plugin.get_widget()
      self.content.pack_end(self.plugin_widget, True)
      self.dialog.show_all()


  def button_share_clicked (self, widget, data = None):
    selection = self.treeview.get_selection()
    (model, iter) = selection.get_selected()
    name = self.liststore.get_value(iter, COLUMN_NAME)
    print "Executing " + name
    plugin = self.plugin_manager.get_by_name(name)
    plugin.share(self.uris)
    # FIXME: quit, do something else
    self.dialog.hide()


  def run (self):

    title = "Sharing " + str(len(self.uris))
    if len(self.uris) > 1:
      title = title + " files"
    else:
      title = title + " file"

    self.dialog = gtk.Dialog(title)
    gtk.window_set_default_icon_name("document-send")

    self.dialog.connect("delete-event", self.close_application)
    self.dialog.set_has_separator(False)
    self.dialog.set_border_width(6)
    self.dialog.set_position(gtk.WIN_POS_CENTER)

    screen = self.dialog.get_screen()
    screen_height = screen.get_height()
    height = 4 * (screen_height / 10)

    self.dialog.set_default_size((height * 8) / 6, height)

    self.content = gtk.HBox(False, 6)
    self.content.set_border_width(6)
#    content_area = self.dialog.get_content_area()
#    content_area.add(self.content)
    self.dialog.vbox.add(self.content)

    button_cancel = gtk.Button(stock=gtk.STOCK_CANCEL)
    button_cancel.connect("clicked", self.close_application)
    button_share = gtk.Button(label="Share")
    button_share.connect("clicked", self.button_share_clicked)

    self.dialog.action_area.pack_start(button_cancel, True, True, 0)
    self.dialog.action_area.pack_start(button_share, True, True, 0)

#    self.dialog.add_buttons(gtk.STOCK_CANCEL, gtk.RESPONSE_CLOSE, "Share", gtk.RESPONSE_ACCEPT)

    scroll = gtk.ScrolledWindow()
    scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
    scroll.set_shadow_type(gtk.SHADOW_IN)
    self.content.pack_start(scroll, False)
    scroll.set_size_request ((height * 8) / 7 / 3, -1)


    self.treeview = gtk.TreeView()
    self.treeview.set_headers_visible(False)
    self.treeview.expand_all()

    self.liststore = gtk.ListStore(str, str)
    self.treeview.set_model(self.liststore)

    img_cell = gtk.CellRendererPixbuf()
    img_column = gtk.TreeViewColumn("Icon", img_cell, **{"icon-name": COLUMN_ICON})
    self.treeview.append_column(img_column)

    text_cell = gtk.CellRendererText()
    text_column = gtk.TreeViewColumn("PluginName", text_cell, text=COLUMN_NAME)
    self.treeview.append_column(text_column)

    self.treeview.connect("cursor-changed", self.on_cursor_changed)

    scroll.add(self.treeview)

    self.plugin_manager = PluginManager()

    for plugin in self.plugin_manager.list_plugins():
      print "FO:" + plugin.name
      if plugin.activated:
        self.liststore.append([plugin.icon, plugin.name])

    self.treeview.columns_autosize()
    self.dialog.show_all()
